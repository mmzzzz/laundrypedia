package myapp.com.laundrypedia.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.InformasiActivity;
import myapp.com.laundrypedia.activity.LoginActivity;
import myapp.com.laundrypedia.activity.PeraturanKomplainActivity;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;

public class ProfileFragment extends Fragment {
    TextView tvnama, tvemail;
    UserHelper_sqlite userHelper_sqlite;
    CardView cvlogout;
    View rootView;
    CardView cvhelp;
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_account, container, false);
        userHelper_sqlite = new UserHelper_sqlite(getActivity().getApplication());
        tvnama = rootView.findViewById(R.id.textView11);
        tvemail = rootView.findViewById(R.id.textView12);
        cvhelp = rootView.findViewById(R.id.CVHelp);
        tvnama.setText(userHelper_sqlite.getobject("nama"));
        tvemail.setText(userHelper_sqlite.getobject("email"));
        cvlogout = rootView.findViewById(R.id.CVLogout);
        cvlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askDelete();

            }
        });

        cvhelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent helpp = new Intent(getContext(), InformasiActivity.class);
                startActivity(helpp);
            }
        });
        return rootView;
    }

    private void askDelete(){
        new MaterialDialog.Builder(getContext())
                .title(R.string.app_name)
                .content("Anda Yakin Ingin Keluar? ")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        userHelper_sqlite.delete("user_member");
                        userHelper_sqlite.delete("tbl_transaksi_sementara");
                        Intent home = new Intent(getContext(), LoginActivity.class);
                        startActivity(home);
                    }
                })
                .show();
    }
}

