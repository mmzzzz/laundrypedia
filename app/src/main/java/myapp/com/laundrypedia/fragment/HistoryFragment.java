package myapp.com.laundrypedia.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.GetChars;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.SemuaLaundryActivity;
import myapp.com.laundrypedia.adapter.adapter_history;
import myapp.com.laundrypedia.adapter.adapter_paket_all;
import myapp.com.laundrypedia.adapter.adapter_paket_laundry;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;
import myapp.com.laundrypedia.model.item_history;
import myapp.com.laundrypedia.model.item_laundry;

public class HistoryFragment extends Fragment {
    private ProgressDialog pDialog;
    private RecyclerView RVHistory;
    private adapter_history madapter;
    private List<item_history> Item_History = new ArrayList<item_history>();
    private static String TAG = "HistoryFragment";

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected HistoryFragment.LayoutManagerType mCurrentLayoutManagerType;
    UserHelper_sqlite userHelper_sqlite;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_history, container, false);
        InitView();

        Req_HomePage();
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());
        userHelper_sqlite = new UserHelper_sqlite(getActivity().getApplication());

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void InitView(){


        RVHistory = (RecyclerView) rootView.findViewById(R.id.RVHistory);
        RVHistory.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getContext(), 1);
        RVHistory.setLayoutManager(mLayoutManager);

        madapter = new adapter_history(getContext(), Item_History);
        RVHistory.setAdapter(madapter);
    }
    private void Req_HomePage(){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_History, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", userHelper_sqlite.getobject("email"));

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }

    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_history item = new item_history();
                item.setNamaLaund(dataArray.getString("store_name"));
                item.setDelivAddr(dataArray.getString("deliver_location"));
                item.setDelivDate(dataArray.getString("deliver_date"));
                item.setDelivTime(dataArray.getString("deliver_time"));
                item.setSubtotal(dataArray.getString("subtotal"));
                item.setStatus(dataArray.getString("id_status_pesanan"));
                item.setNo_faktur(dataArray.getString("no_faktur"));
                item.setStat_desc(dataArray.getString("status_name"));
                item.setIDLaundry(dataArray.getString("id_binatu"));
                item.setOngkir(dataArray.getString("ongkir"));
                //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                Item_History.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}