package myapp.com.laundrypedia.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.SemuaLaundryActivity;
import myapp.com.laundrypedia.adapter.adapter_paket_laundry;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.model.item_laundry;

public class HomeFragment extends Fragment {
    private ProgressDialog pDialog;
    private HorizontalGridView HGV_Laundry;
    private adapter_paket_laundry madapter;
    private List<item_laundry> Item_Laundry = new ArrayList<item_laundry>();
    private static String TAG = "HomeFragment";
    Button btnseeall;
    View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        InitView();

        Req_HomePage();
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pDialog = new ProgressDialog(getActivity());

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void InitView(){
        btnseeall = rootView.findViewById(R.id.SeeAllTerbaru);
        btnseeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent All = new Intent(getActivity(), SemuaLaundryActivity.class);
                startActivity(All);
            }
        });


        HGV_Laundry = (HorizontalGridView) rootView.findViewById(R.id.HGV_PaketTerbaru);
        madapter = new adapter_paket_laundry(getActivity(), Item_Laundry);
        HGV_Laundry.setAdapter(madapter);
    }
    private void Req_HomePage(){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_HomePage, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key", "1234");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }

    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_laundry item = new item_laundry();

                item.setNamaLaundry(dataArray.getString("store_name"));
                item.setAlamatLaundry(dataArray.getString("address"));
                item.setGambarLaundry(dataArray.getString("logo"));
                item.setLongLaundry(dataArray.getString("longitude"));
                item.setLatLaundry(dataArray.getString("latitude"));
                item.setJamClose(dataArray.getString("close_at"));
                item.setJamOpen(dataArray.getString("open_at"));
                item.setIdLaundry(dataArray.getString("id_binatu"));
                item.setBiayaOngkir(dataArray.getString("shipping_cost"));
                item.setJarakFreeOngkir(dataArray.getString("free_on_charge"));
               //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                Item_Laundry.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}