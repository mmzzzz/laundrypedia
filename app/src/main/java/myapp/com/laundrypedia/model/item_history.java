package myapp.com.laundrypedia.model;

public class item_history  {
    private String IDLaundry;
    private String NamaLaund;
    private String DelivAddr;
    private String DelivDate;
    private String DelivTime;

    private String PickAddr;
    private String PickDate;
    private String PickTime;

    private String status;
    private String subtotal;

    private String no_faktur;
    private String stat_desc;

    private String Ongkir;

    public String getOngkir() {
        return Ongkir;
    }

    public String getIDLaundry() {
        return IDLaundry;
    }

    public String getStat_desc() {
        return stat_desc;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public String getStatus() {
        return status;
    }

    public String getDelivDate() {
        return DelivDate;
    }

    public String getDelivAddr() {
        return DelivAddr;
    }

    public String getDelivTime() {
        return DelivTime;
    }

    public String getNamaLaund() {
        return NamaLaund;
    }

    public String getPickAddr() {
        return PickAddr;
    }

    public String getPickDate() {
        return PickDate;
    }

    public String getPickTime() {
        return PickTime;
    }


    public void setDelivAddr(String delivAddr) {
        DelivAddr = delivAddr;
    }

    public void setDelivDate(String delivDate) {
        DelivDate = delivDate;
    }

    public void setDelivTime(String delivTime) {
        DelivTime = delivTime;
    }

    public void setNamaLaund(String namaLaund) {
        NamaLaund = namaLaund;
    }

    public void setPickAddr(String pickAddr) {
        PickAddr = pickAddr;
    }

    public void setPickDate(String pickDate) {
        PickDate = pickDate;
    }

    public void setPickTime(String pickTime) {
        PickTime = pickTime;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getNo_faktur() {
        return no_faktur;
    }

    public void setNo_faktur(String no_faktur) {
        this.no_faktur = no_faktur;
    }

    public void setStat_desc(String stat_desc) {
        this.stat_desc = stat_desc;
    }

    public void setIDLaundry(String IDLaundry) {
        this.IDLaundry = IDLaundry;
    }

    public void setOngkir(String ongkir) {
        Ongkir = ongkir;
    }
}
