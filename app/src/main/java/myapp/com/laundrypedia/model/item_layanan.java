package myapp.com.laundrypedia.model;

public class item_layanan {
    private String IdLayanan;
    private String NamaLy;
    private String HargaLy;
    private String DescLay;
    private String GambarLy;

    private String idbinatu;
    private String Satuan;

    public String getSatuan() {
        return Satuan;
    }

    public String getGambarLy() {
        return GambarLy;
    }

    public String getDescLay() {
        return DescLay;
    }

    public String getHargaLy() {
        return HargaLy;
    }

    public String getIdLayanan() {
        return IdLayanan;
    }

    public String getNamaLy() {
        return NamaLy;
    }

    public String getIdbinatu() {
        return idbinatu;
    }

    public void setDescLay(String descLay) {
        DescLay = descLay;
    }

    public void setHargaLy(String hargaLy) {
        HargaLy = hargaLy;
    }

    public void setIdLayanan(String idLayanan) {
        IdLayanan = idLayanan;
    }

    public void setNamaLy(String namaLy) {
        NamaLy = namaLy;
    }

    public void setGambarLy(String gambarLy) {
        GambarLy = gambarLy;
    }
    public void setIdbinatu(String idbinatu) {
        this.idbinatu = idbinatu;
    }

    public void setSatuan(String satuan) {
        Satuan = satuan;
    }
}
