package myapp.com.laundrypedia.model;

public class item_detail_layanan {
    private String NamaDl;
    private String JmlDL;
    private String HargaDL;

    public String getHargaDL() {
        return HargaDL;
    }

    public String getJmlDL() {
        return JmlDL;
    }

    public String getNamaDl() {
        return NamaDl;
    }

    public void setHargaDL(String hargaDL) {
        HargaDL = hargaDL;
    }

    public void setJmlDL(String jmlDL) {
        JmlDL = jmlDL;
    }

    public void setNamaDl(String namaDl) {
        NamaDl = namaDl;
    }
}
