package myapp.com.laundrypedia.model;

public class item_laundry  {
    private String GambarLaundry;
    private String NamaLaundry;
    private String JarakLaundry;
    private String AlamatLaundry;
    private String JamLaundry;
    private String LongLaundry;
    private String LatLaundry;
    private String JamOpen;
    private String JamClose;
    private String IdLaundry;
    private String NoFaktur;
    private String BiayaOngkir;
    private String JarakFreeOngkir;
    private String Status;

    public String getStatus() {
        return Status;
    }

    public String getJarakFreeOngkir() {
        return JarakFreeOngkir;
    }

    public String getBiayaOngkir() {
        return BiayaOngkir;
    }

    public String getAlamatLaundry() {
        return AlamatLaundry;
    }

    public String getJarakLaundry() {
        return JarakLaundry;
    }

    public String getNamaLaundry() {
        return NamaLaundry;
    }

    public String getJamLaundry() {
        return JamLaundry;
    }

    public String getGambarLaundry() {
        return GambarLaundry;
    }

    public String getLatLaundry() {
        return LatLaundry;
    }

    public String getLongLaundry() {
        return LongLaundry;
    }

    public String getJamClose() {
        return JamClose;
    }

    public String getJamOpen() {
        return JamOpen;
    }

    public String getIdLaundry() {
        return IdLaundry;
    }

    public void setAlamatLaundry(String alamatLaundry) {
        AlamatLaundry = alamatLaundry;
    }

    public void setJarakLaundry(String jarakLaundry) {
        JarakLaundry = jarakLaundry;
    }

    public void setNamaLaundry(String namaLaundry) {
        NamaLaundry = namaLaundry;
    }

    public void setGambarLaundry(String gambarLaundry) {
        GambarLaundry = gambarLaundry;
    }

    public void setJamLaundry(String jamLaundry) {
        JamLaundry = jamLaundry;
    }

    public void setLatLaundry(String latLaundry) {
        LatLaundry = latLaundry;
    }

    public void setLongLaundry(String longLaundry) {
        LongLaundry = longLaundry;
    }

    public void setJamClose(String jamClose) {
        JamClose = jamClose;
    }

    public void setJamOpen(String jamOpen) {
        JamOpen = jamOpen;
    }

    public void setIdLaundry(String idLaundry) {
        IdLaundry = idLaundry;
    }

    public void setBiayaOngkir(String biayaOngkir) {
        BiayaOngkir = biayaOngkir;
    }

    public void setJarakFreeOngkir(String jarakFreeOngkir) {
        JarakFreeOngkir = jarakFreeOngkir;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
