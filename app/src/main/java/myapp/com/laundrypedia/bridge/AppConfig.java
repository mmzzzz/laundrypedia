package myapp.com.laundrypedia.bridge;

public class AppConfig  {

    public static String HOST = "http://laundrypedia.store/androidnew/";
    public static String URL_Login = HOST+"mlogin.php";
    public static String URL_Register = HOST+"mregister.php";
    public static String URL_HomePage = HOST+"mshowpakethome.php";
    public static String URL_SemuaLaundry= HOST+"mshowlaundry.php";
    public static String URL_Layanan = HOST+"getlayanan.php";
    public static String URL_InputOrder = HOST+"inputorder.php";
    public static String URL_History = HOST+"gethistory.php";
    public static String URL_Konfirm = HOST+"sendkonfirm.php";
    public static String URL_get_detail = HOST+"getdetail.php";
    public static String URL_SENDKOMPLAIN = HOST+"sendkomplain.php";

}
