package myapp.com.laundrypedia.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.DetailStoreActivity;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;
import myapp.com.laundrypedia.model.item_laundry;
import myapp.com.laundrypedia.model.item_layanan;

public class adapter_layanan extends RecyclerView.Adapter<adapter_layanan.MyViewHolder> {
    private List<item_layanan> ListLayanan;
    private Context mContext;
    Button plus, min, btnok;
    Integer nol;
    EditText jml, edtotal;

    int j0;
    UserHelper_sqlite userHelper_sqlite;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama;
        public final TextView tvHarga;
        public final ImageView imglayanan;
        public final Button btnadd;
    //    public final RelativeLayout RVpaket;


        public MyViewHolder(View view) {
            super(view);
            tvNama = (TextView) view.findViewById(R.id.tvnamalayanan);
            tvHarga = (TextView) view.findViewById(R.id.tvhargalyn);
            imglayanan = (ImageView) view.findViewById(R.id.imglayanan);
            btnadd = (Button) view.findViewById(R.id.btnadd);
           // RVpaket = (RelativeLayout) view.findViewById(R.id.RV1);
        }
    }

    public adapter_layanan(Context context, List<item_layanan> ListLayanan) {
        mContext = context;
        this.ListLayanan= ListLayanan;
        userHelper_sqlite = new UserHelper_sqlite(context);
    }

    @Override
    public adapter_layanan.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_layanan, parent, false);

        return new adapter_layanan.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final adapter_layanan.MyViewHolder holder, int position) {
        final item_layanan item = ListLayanan.get(position);

        holder.tvNama.setText(item.getNamaLy());
        holder.tvHarga.setText("Rp. "+item.getHargaLy() +" /"+item.getSatuan());
       Glide
                .with(mContext)
                .load(item.getGambarLy())
                .asBitmap()
                .placeholder(R.drawable.cucisetrika)
                .into(holder.imglayanan);

        holder.btnadd.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //muncul dialog
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(v.getRootView().getContext());
                alertDialogBuilder.setTitle("Input Jumlah order");
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.dialog_add,null);
                alertDialogBuilder.setView(dialogView);

                plus = (Button) dialogView.findViewById(R.id.btnplus);
                min = (Button) dialogView.findViewById(R.id.btnmin);

                jml = (EditText) dialogView.findViewById(R.id.edjumlah);
                btnok = (Button) dialogView.findViewById(R.id.button6);

                int nol = Integer.parseInt(jml.getText().toString());
                if(nol == 0){
                    min.setEnabled(false);
                }
                edtotal = (EditText) dialogView.findViewById(R.id.edtotal);
                j0 = Integer.parseInt(jml.getText().toString());
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                plus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // int harga = Integer.parseInt(myHolder.textPrice.getText().toString());
                        int ang = Integer.parseInt(jml.getText().toString());
                        int angka = ang +1;
                        int har = Integer.parseInt(item.getHargaLy());

                        jml.setText(""+angka);

                        edtotal.setText(""+angka*har);
                        min.setEnabled(true);


                        if(angka >  15){
                            Toast.makeText(mContext, "Maaf Jumlah yang Anda Masukkan melebihi Batas Kuota", Toast.LENGTH_LONG).show();
                            edtotal.setText(""+0);
                            jml.setText(""+0);

                        }



                    }
                });
                min.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int ang = Integer.parseInt(jml.getText().toString());
                        int angka = ang -1;
                        jml.setText(""+angka);
                        int har = Integer.parseInt(item.getHargaLy());


                        edtotal.setText(""+angka*har);
                        if(angka < 0){
                            Toast.makeText(mContext, "tidak boleh minus", Toast.LENGTH_LONG).show();
                            edtotal.setText(""+0);
                            jml.setText(""+0);

                        }


                    }
                });

                btnok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userHelper_sqlite.save_trans_sementara(item.getIdbinatu(),item.getIdLayanan(),item.getNamaLy(),jml.getText().toString(),item.getHargaLy(),edtotal.getText().toString());
                        alertDialog.dismiss();
                    }
                });


                //Showing the alert dialog

            }


        });

    }

    @Override
    public int getItemCount() {
        return ListLayanan.size();
    }


}



