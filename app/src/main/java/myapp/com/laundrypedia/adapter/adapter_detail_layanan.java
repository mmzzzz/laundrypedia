package myapp.com.laundrypedia.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.model.item_detail_layanan;
import myapp.com.laundrypedia.model.item_laundry;

public class adapter_detail_layanan extends RecyclerView.Adapter<adapter_detail_layanan.MyViewHolder> {
    private List<item_detail_layanan> ListDetail;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama;
        public final TextView tvHarga;
        public final TextView tvJumlah;

        public MyViewHolder(View view) {
            super(view);
            tvNama = (TextView) view.findViewById(R.id.tvnamalayanan);
            tvHarga = (TextView) view.findViewById(R.id.tvhargalyn);
            tvJumlah = (TextView) view.findViewById(R.id.tvjumlah);

        }
    }

    public adapter_detail_layanan(Context context, List<item_detail_layanan> ListDetail) {
        mContext = context;
        this.ListDetail= ListDetail;
    }

    @Override
    public adapter_detail_layanan.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_detail, parent, false);

        return new adapter_detail_layanan.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_detail_layanan.MyViewHolder holder, int position) {
        final item_detail_layanan item = ListDetail.get(position);

        holder.tvNama.setText(item.getNamaDl());
        holder.tvHarga.setText("Rp. "+item.getHargaDL());
        holder.tvJumlah.setText(item.getJmlDL());

    }

    @Override
    public int getItemCount() {
        return ListDetail.size();
    }


}



