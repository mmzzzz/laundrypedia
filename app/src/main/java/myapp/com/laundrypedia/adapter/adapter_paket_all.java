package myapp.com.laundrypedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.DetailStoreActivity;
import myapp.com.laundrypedia.helper.GPSTracker;
import myapp.com.laundrypedia.model.item_laundry;

public class adapter_paket_all extends RecyclerView.Adapter<adapter_paket_all.MyViewHolder> implements Filterable {
    private List<item_laundry> ListLaundry;
    private Context mContext;
    private List<item_laundry> ListLaundryFiltered;
    GPSTracker gpsTracker;


    float ongkir;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama;
        public final TextView tvjarak;
        public final TextView tvJam;
        public final ImageView imglaundry;
        public final RelativeLayout RVpaket;


        public MyViewHolder(View view) {
            super(view);
            tvNama = (TextView) view.findViewById(R.id.tvnama);
            tvjarak = (TextView) view.findViewById(R.id.tvjarakalamat);
            tvJam = (TextView) view.findViewById(R.id.tvjam);
            imglaundry = (ImageView) view.findViewById(R.id.imglaundry);
            RVpaket = (RelativeLayout) view.findViewById(R.id.RV1);
        }
    }

    public adapter_paket_all(Context context, List<item_laundry> ListLaundry) {
        mContext = context;
        this.ListLaundry = ListLaundry;
        this.ListLaundryFiltered  = ListLaundry;
        gpsTracker = new GPSTracker(mContext);
    }

    @Override
    public adapter_paket_all.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_laundry, parent, false);

        return new adapter_paket_all.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_paket_all.MyViewHolder holder, int position) {
        final item_laundry item = ListLaundryFiltered.get(position);

        holder.tvNama.setText(item.getNamaLaundry());

        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            //String stringLatitude1 = ;
            //String stringLongitude1 = String.valueOf(gpsTracker.getLongitude());

            Double lat1 = gpsTracker.getLatitude();
            Double lng1 = gpsTracker.getLongitude();

            Double lat2 = Double.parseDouble(item.getLatLaundry());
            Double lng2 = Double.parseDouble(item.getLongLaundry());


            Location loc1 = new Location("");
            loc1.setLatitude(lat1);
            loc1.setLongitude(lng1);

            Location loc2 = new Location("");
            loc2.setLatitude(lat2);
            loc2.setLongitude(lng2);

            float distanceInMeters = loc1.distanceTo(loc2);
            float distance_km = distanceInMeters / 1000 ;
            int dis = (int) distance_km;


           // String jrk = String.format("%.2f",distance_km);
            holder.tvjarak.setText((String.format("%.2f",distance_km))+" km . "+ item.getAlamatLaundry());

            float ongkirdasar = Float.parseFloat(item.getBiayaOngkir());
            float jarakfree = Float.parseFloat(item.getJarakFreeOngkir());

            if(distance_km > jarakfree){
                ongkir = distance_km * ongkirdasar;
            }
               else{
                ongkir = 0 ;
            }

            Log.d("JARAKKK :", ""+distance_km);


        }
        else
        {
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }


        holder.tvJam.setText(item.getJamOpen() + "-" + item.getJamClose());
        Glide
                .with(mContext)
                .load(item.getGambarLaundry())
                .asBitmap()
                .placeholder(R.drawable.ic_slide2)
                .into(holder.imglaundry);

        holder.RVpaket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Detail = new Intent(mContext, DetailStoreActivity.class);
                Detail.putExtra("idbinatu", item.getIdLaundry());
                Detail.putExtra("gambar", item.getGambarLaundry());
                Detail.putExtra("nama", item.getNamaLaundry());
                Detail.putExtra("alamat", item.getAlamatLaundry());
                Detail.putExtra("jam_open", item.getJamOpen());
                Detail.putExtra("jam_close", item.getJamClose());
                Detail.putExtra("ongkir", ""+ongkir);
                Detail.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(Detail);
            }
        });

    }

    @Override
    public int getItemCount() {
        return ListLaundryFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    ListLaundryFiltered = ListLaundry;
                } else {
                    List<item_laundry> filteredList = new ArrayList<>();
                    for (item_laundry row : ListLaundry) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getNamaLaundry().toLowerCase().contains(charString.toLowerCase()) || row.getAlamatLaundry().contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    ListLaundryFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = ListLaundryFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                ListLaundryFiltered = (ArrayList<item_laundry>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }


}



