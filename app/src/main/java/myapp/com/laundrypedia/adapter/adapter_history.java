package myapp.com.laundrypedia.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.activity.DetailNotaActivity;
import myapp.com.laundrypedia.activity.DetailStoreActivity;
import myapp.com.laundrypedia.activity.MainActivity;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.model.item_history;
import myapp.com.laundrypedia.model.item_laundry;

import static android.support.constraint.Constraints.TAG;

public class adapter_history extends RecyclerView.Adapter<adapter_history.MyViewHolder> {
    private List<item_history> ListHistory;
    private Context mContext;
    Button konfirm;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama;
        public final TextView tvalamat;
        public final TextView tvJam;
        public final ImageView imglaundry;
        public final Button subtotal;
        public final Button btncom;
        public final Button btnprog;
        public final LinearLayout LLHistory;
       // public final RelativeLayout RVpaket;
        public final Button txtstatus;
        public final Button btnkonfirm;
        public final TextView txtnota;



        public MyViewHolder(View view) {
            super(view);
            tvNama = (TextView) view.findViewById(R.id.tvnama);
            tvalamat = (TextView) view.findViewById(R.id.textView9);
            tvJam = (TextView) view.findViewById(R.id.textView10);
            subtotal = (Button) view.findViewById(R.id.button5);
            btncom = (Button) view.findViewById(R.id.btncomplete);
            btnprog = (Button) view.findViewById(R.id.btnprogress);

            imglaundry = (ImageView) view.findViewById(R.id.imageView);
            LLHistory= (LinearLayout) view.findViewById(R.id.LLHistory);
            txtstatus = (Button) view.findViewById(R.id.txtstatus);
            btnkonfirm = (Button) view.findViewById(R.id.btnconfirm);
            txtnota = (TextView) view.findViewById(R.id.txtnota);


        }
    }

    public adapter_history(Context context, List<item_history> ListHistory) {
        mContext = context;
        this.ListHistory= ListHistory;
    }

    @Override
    public adapter_history.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_history, parent, false);

        return new adapter_history.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_history.MyViewHolder holder, int position) {
        final item_history item = ListHistory.get(position);

        holder.tvNama.setText(item.getNamaLaund());
        holder.tvalamat.setText(item.getDelivAddr());
        holder.tvJam.setText(item.getDelivDate() +" "+ item.getDelivTime());
        holder.subtotal.setText(item.getSubtotal());
       /* Glide
                .with(mContext)
                .load(item.get())
                .asBitmap()
                .placeholder(R.drawable.ic_slide2)
                .into(holder.imglaundry);*/

       holder.txtstatus.setText(item.getStat_desc());
        holder.btnkonfirm.setVisibility(View.GONE);

        if(item.getStatus().equals("1") || item.getStatus().equals("2") ){
            holder.txtnota.setVisibility(View.GONE);
            //holder.btnkonfirm.setVisibility(View.GONE);
           // holder.txtstatus.setText("On Process");

        }
        else if(item.getStatus().equals("3")) {
         //   holder.txtstatus.setText("Delivered");
            holder.txtnota.setVisibility(View.GONE);
            /*holder.btnkonfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //dialog konfirmasi
                    //muncul dialog
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getRootView().getContext());
                    alertDialogBuilder.setTitle("Konfirmasi Pesanan");
                    LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_add,null);
                    alertDialogBuilder.setView(dialogView);

                    konfirm = (Button) dialogView.findViewById(R.id.button6);

                    konfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            //kirim
                            Req_konfirm(item.getNo_faktur());
                        }
                    });



                    alertDialogBuilder.setNegativeButton("Batal",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    // form_input();
                                    //

                                }


                            });
                    //Showing the alert dialog

                }
            });*/

        }

        else if(item.getStatus().equals("5")){
          //  holder.txtstatus.setText("Completed");

            holder.btncom.setVisibility(View.GONE);

            holder.txtnota.setVisibility(View.VISIBLE);
            holder.txtnota.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent Detail = new Intent(mContext, DetailNotaActivity.class);
                    Detail.putExtra("nofaktur", item.getNo_faktur());
                    Detail.putExtra("subtotal", item.getSubtotal());
                    Detail.putExtra("idlaundry", item.getIDLaundry());
                    Detail.putExtra("status", item.getStatus());
                    Detail.putExtra("ongkir", item.getOngkir());

                    mContext.startActivity(Detail);

                }
            });


        }
        holder.LLHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Detail = new Intent(mContext, DetailNotaActivity.class);
                Detail.putExtra("nofaktur", item.getNo_faktur());
                Detail.putExtra("subtotal", item.getSubtotal());
                Detail.putExtra("idlaundry", item.getIDLaundry());
                Detail.putExtra("status", item.getStatus());
                Detail.putExtra("ongkir", item.getOngkir());



                mContext.startActivity(Detail);

            }
        });

    }

    @Override
    public int getItemCount() {
        return ListHistory.size();
    }

    private void Req_konfirm(final String nofaktur){
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_Konfirm, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                            //save
                        DialogPesan("Notification", "Success Confirmed");

                    }
                    else {
                        DialogPesan("Login Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nofaktur", nofaktur);



                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);
    }


    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(mContext)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }
}



