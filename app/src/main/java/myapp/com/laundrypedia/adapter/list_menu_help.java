package myapp.com.laundrypedia.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import myapp.com.laundrypedia.R;

public class list_menu_help extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] itemname;


    public list_menu_help(Activity context, String[] itemname) {
        super(context, R.layout.adapter_help, itemname);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.itemname=itemname;

    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.adapter_help, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.tv_judulhelp);
       txtTitle.setText(itemname[position]);

        return rowView;

    };
}
