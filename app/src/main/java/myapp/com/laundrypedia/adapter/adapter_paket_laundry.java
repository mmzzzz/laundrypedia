package myapp.com.laundrypedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.helper.GPSTracker;
import myapp.com.laundrypedia.model.item_laundry;

public class adapter_paket_laundry extends RecyclerView.Adapter<adapter_paket_laundry.MyViewHolder> {
    private List<item_laundry> ListLaundry;
    private Context mContext;
    GPSTracker gpsTracker;
    double ongkir;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvNama;
        public final TextView tvjarak;
        public final TextView tvJam;
        public final ImageView imglaundry;

        public MyViewHolder(View view) {
            super(view);
            tvNama = (TextView) view.findViewById(R.id.tvnama);
            tvjarak = (TextView) view.findViewById(R.id.tvjarakalamat);
            tvJam = (TextView) view.findViewById(R.id.tvjam);
            imglaundry = (ImageView) view.findViewById(R.id.imglaundry);

        }
    }

    public adapter_paket_laundry(Context context, List<item_laundry> ListLaundry) {
        mContext = context;
        this.ListLaundry= ListLaundry;
        gpsTracker = new GPSTracker(mContext);
    }

    @Override
    public adapter_paket_laundry.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_home, parent, false);

        return new adapter_paket_laundry.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(adapter_paket_laundry.MyViewHolder holder, int position) {
        final item_laundry item = ListLaundry.get(position);

        holder.tvNama.setText(item.getNamaLaundry());
        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            //String stringLatitude1 = ;
            //String stringLongitude1 = String.valueOf(gpsTracker.getLongitude());

            Double lat1 = gpsTracker.getLatitude();
            Double lng1 = gpsTracker.getLongitude();

            Double lat2 = Double.parseDouble(item.getLatLaundry());
            Double lng2 = Double.parseDouble(item.getLongLaundry());


            Location loc1 = new Location("");
            loc1.setLatitude(lat1);
            loc1.setLongitude(lng1);

            Location loc2 = new Location("");
            loc2.setLatitude(lat2);
            loc2.setLongitude(lng2);

            float distanceInMeters = loc1.distanceTo(loc2);
            float distance_km = distanceInMeters / 1000 ;
            int dis = (int) distance_km;


            // String jrk = String.format("%.2f",distance_km);
            holder.tvjarak.setText((String.format("%.2f",distance_km))+" km . "+ item.getAlamatLaundry());

            float ongkirdasar = Float.parseFloat(item.getBiayaOngkir());
            float jarakfree =Float.parseFloat(item.getJarakFreeOngkir());

            if(distance_km > jarakfree){
                ongkir = distance_km * ongkirdasar;
            }
            else{
                ongkir = ongkirdasar;
            }

            Log.d("JARAKKK :", ""+distance_km);

        }
        else
        {
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }


       // holder.tvjarak.setText("0.18 km"+" . "+item.getAlamatLaundry());
        holder.tvJam.setText(item.getJamOpen() +"-"+ item.getJamClose());
        Glide
                .with(mContext)
                .load(item.getGambarLaundry())
                .asBitmap()
                .placeholder(R.drawable.ic_slide2)
                .into(holder.imglaundry);

    }

    @Override
    public int getItemCount() {
        return ListLaundry.size();
    }


}


