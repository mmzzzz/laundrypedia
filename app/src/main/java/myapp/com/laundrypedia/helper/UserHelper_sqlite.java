package myapp.com.laundrypedia.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;

public class UserHelper_sqlite extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "LAUNDRYPEDIA"; //DB name

    public UserHelper_sqlite(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //MEMBUAT TABEL USER
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS user_member(email VARCHAR, nama VARCHAR);");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS tbl_transaksi_sementara(idbinatu VARCHAR, idlayanan VARCHAR,nama_layanan VARCHAR, jumlah VARCHAR, harga VARCHAR, harga_total VARCHAR);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //SIMPAN user
    public void save_user(String email, String nama){
        SQLiteDatabase db = this.getWritableDatabase();

        String query="INSERT INTO user_member(email,nama) VALUES " +
                "('"+email+"','"+nama+"');";
        db.execSQL(query);

        db.close();
    }

    public void save_trans_sementara(String idbinatu, String idlayanan, String namalayanan, String jumlah, String harga, String harga_tot){
        SQLiteDatabase db = this.getWritableDatabase();

        String query="INSERT INTO tbl_transaksi_sementara(idbinatu,idlayanan,nama_layanan,jumlah, harga, harga_total) VALUES " +
                "('"+idbinatu+"','"+idlayanan+"','"+namalayanan+"','"+jumlah+"','"+harga+"','"+harga_tot+"');";
        db.execSQL(query);

        db.close();
    }
    //update user


    //UNTUK DELETE
    public void delete(String nama_tbl){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM "+nama_tbl;
        db.execSQL(query);
        db.close();
    }

    public String getobject(String strobject){
        SQLiteDatabase db = this.getWritableDatabase();
        String res = null;
        String selectQuery = "SELECT "+strobject+" from user_member";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount()>0)
        {
            cursor.moveToFirst();
            do {
                res = cursor.getString(0);

            } while (cursor.moveToNext());
        }
        db.close();
        return res;
    }
    //UPDATE VERSIKU
    public void update_member(String nama, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE user_member SET email = '"+email+"', nama='"+nama+"'";
        db.execSQL(query);
        db.close();
    }

    public String gettotalharga(){
        SQLiteDatabase db = this.getWritableDatabase();
        String res = null;
        String selectQuery = "SELECT SUM(harga*jumlah) AS total from tbl_transaksi_sementara";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount()>0)
        {
            cursor.moveToFirst();
            do {
               // res = cursor.getString(cursor.getColumnIndex("tot"));
                res = cursor.getString(cursor.getColumnIndex("total"));

            } while (cursor.moveToNext());
        }
        db.close();
        return res;
    }



    //get isi sqlite
    public String composeJSONfromSQLite() {
        ArrayList<HashMap<String, String>> offlineList;
        offlineList = new ArrayList<HashMap<String, String>>();
        String selectQuery = "SELECT  * FROM tbl_transaksi_sementara ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("idbinatu", cursor.getString(0));
                map.put("idlayanan", cursor.getString(1));
                map.put("nama_layanan", cursor.getString(2));
                map.put("jumlah", cursor.getString(3));
                map.put("harga", cursor.getString(4));
                map.put("harga_total",cursor.getString(5));
                offlineList.add(map);

            } while (cursor.moveToNext());
        }
        db.close();
        Gson gson = new GsonBuilder().create();

        return gson.toJson(offlineList);
    }

}

