package myapp.com.laundrypedia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;

public class FormKomplainActivity extends AppCompatActivity {
    Button btnkirim;
    EditText ednama, edsubtotal, edkomplain;
    ProgressDialog pDialog;
    private static String TAG="KomplainActivity";
    UserHelper_sqlite userHelper_sqlite;
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
          getSupportActionBar().setDisplayHomeAsUpEnabled(true);
          getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
          SetJudul("Form Komplain");

        setContentView(R.layout.activity_form_komplain);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());

        ednama = findViewById(R.id.editText7);
        edsubtotal = findViewById(R.id.editText8);
        edkomplain = findViewById(R.id.editText9);
        btnkirim = findViewById(R.id.button11);

        pDialog = new ProgressDialog(this);
        btnkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                if(edkomplain.getText().toString().length() < 1){
                    Toast.makeText(getApplicationContext(),"Mohon Lengkapi Isian !",Toast.LENGTH_LONG).show();
                }
                else{
                    ReqKomplain(ednama.getText().toString(), edsubtotal.getText().toString(), edkomplain.getText().toString());
                }
            }
        });




    }

    private void ReqKomplain(final String strnama, final String strtotal, final String strkomplain){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_SENDKOMPLAIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                      DialogPesan("Sukses","Komplain Anda telah disampaikan ke pihak Laundry. Mohon menunggu Informasi selanjutnya dari kami maksimal 2x24 jam. Terima Kasih");
                    }
                    else {
                        DialogPesan("Login Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("email",userHelper_sqlite.getobject("email") );
                params.put("faktur", getIntent().getStringExtra("nofaktur"));
                params.put("idlaundry", getIntent().getStringExtra("idlaundry"));
                params.put("nama", strnama);
                params.put("total", strtotal);
                params.put("komplain", strkomplain);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
}
