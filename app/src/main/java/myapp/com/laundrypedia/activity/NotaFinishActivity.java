package myapp.com.laundrypedia.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.adapter_detail_layanan;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;
import myapp.com.laundrypedia.model.item_detail_layanan;

public class NotaFinishActivity extends AppCompatActivity {
    RecyclerView RVdetail;
    adapter_detail_layanan madapter;
    private List<item_detail_layanan> Item_Laundry = new ArrayList<item_detail_layanan>();
    ProgressDialog pDialog;
    private static String TAG="DetailLayanan";
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    UserHelper_sqlite userHelper_sqlite;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected NotaFinishActivity.LayoutManagerType mCurrentLayoutManagerType;
    TextView tvnamauser, tvtotal, tvtanggal;

    Date c;
    String formattedDate;
    SimpleDateFormat df;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("No Pesanan: "+getIntent().getStringExtra("nofaktur"));
        pDialog = new ProgressDialog(this);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        Req_HomePage(getIntent().getStringExtra("nofaktur"));
        setContentView(R.layout.activity_nota_finish);
        InitView();

        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void InitView(){
        tvnamauser = findViewById(R.id.textView4);
        tvtotal     = findViewById(R.id.textView17);
        tvtanggal = findViewById(R.id.textView22);

        tvnamauser.setText("Halo "+userHelper_sqlite.getobject("nama"));
        tvtotal.setText("Total Dibayar : Rp. "+getIntent().getStringExtra("subtotal")+",00");
        tvtanggal.setText("Salam, Laundrypedia");

        RVdetail = findViewById(R.id.LVlist);

        RVdetail.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RVdetail.setLayoutManager(mLayoutManager);

        madapter = new adapter_detail_layanan(getApplicationContext(), Item_Laundry);
        RVdetail.setAdapter(madapter);
    }

    private void Req_HomePage(final String nofaktur){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_get_detail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("nofaktur", nofaktur);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_detail_layanan item = new item_detail_layanan();

                item.setNamaDl(dataArray.getString("nama_layanan"));
                item.setJmlDL(dataArray.getString("qty"));
                item.setHargaDL(dataArray.getString("item_price"));

                //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                Item_Laundry.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
