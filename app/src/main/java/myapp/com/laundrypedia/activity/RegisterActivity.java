package myapp.com.laundrypedia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;

public class RegisterActivity extends AppCompatActivity {
    TextView linktolog;
    Button btnreg;
    EditText edname, edphone, edemail, edpassword;
    ProgressDialog pDialog;
    private static String TAG="RegisterActivity";
    UserHelper_sqlite userHelper_sqlite;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
   /*     getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        // SetJudul("All Laundry Stores");*/

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());

        edname = findViewById(R.id.edr_nama);
        edphone = findViewById(R.id.ednohp);
        edemail = findViewById(R.id.edr_email);
        edpassword = findViewById(R.id.ed_password);
        btnreg = findViewById(R.id.btn_reg);
        linktolog = findViewById(R.id.tvtolog);
        pDialog = new ProgressDialog(this);
        btnreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                if(edemail.getText().toString().length() < 1 || edphone.getText().toString().length() < 1 || edpassword.getText().toString().length()<1 || edname.getText().toString().length() < 1){
                    Toast.makeText(getApplicationContext(),"Mohon Lengkapi Isian !",Toast.LENGTH_LONG).show();
                }
                else{
                    SendRegis(edname.getText().toString(), edphone.getText().toString(),edemail.getText().toString(), edpassword.getText().toString());
                }
            }
        });


        //
        linktolog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dash = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(dash);
            }
        });

    }

    private void SendRegis(final String fullname, final String phone, final String stremail, final String strpassword){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_Register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){

                        //save
                        userHelper_sqlite.save_user(stremail,fullname);
                        Intent dash = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(dash);
                        finish();
                    }
                    else {
                        DialogPesan("Register Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("email", stremail);
                params.put("password", strpassword);
                params.put("fullname", fullname);
                params.put("phone", phone);


                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                    }
                })
                .show();
    }
}

