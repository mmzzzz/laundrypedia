package myapp.com.laundrypedia.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.adapter_detail_layanan;
import myapp.com.laundrypedia.adapter.adapter_paket_all;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;
import myapp.com.laundrypedia.model.item_detail_layanan;
import myapp.com.laundrypedia.model.item_laundry;

import static android.support.constraint.Constraints.TAG;

public class DetailNotaActivity extends AppCompatActivity {
    RecyclerView RVdetail;
    adapter_detail_layanan madapter;
    private List<item_detail_layanan> Item_Laundry = new ArrayList<item_detail_layanan>();
    ProgressDialog pDialog;
    private static String TAG="DetailLayanan";
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    UserHelper_sqlite userHelper_sqlite;
    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected DetailNotaActivity.LayoutManagerType mCurrentLayoutManagerType;
    TextView tvnamauser, tvtotal, tvtanggal;

    Date c;
    String formattedDate;
    SimpleDateFormat df;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    Button btnkonfirm, btnkomplain;
    Button konfirm;
    TextView tvongkir;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("No Pesanan: "+getIntent().getStringExtra("nofaktur"));
        pDialog = new ProgressDialog(this);
        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        Req_HomePage(getIntent().getStringExtra("nofaktur"));
        setContentView(R.layout.activity_nota);
        InitView();

        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void InitView(){
        btnkonfirm = findViewById(R.id.button4);

        if(getIntent().getStringExtra("status").equals("3")){
            btnkonfirm.setVisibility(View.VISIBLE);
        }
        else {
            btnkonfirm.setVisibility(View.GONE);
        }
        btnkomplain = findViewById(R.id.button8);


        tvnamauser = findViewById(R.id.textView4);
        tvtotal     = findViewById(R.id.textView17);
        tvtanggal = findViewById(R.id.textView22);
        tvongkir = findViewById(R.id.tvongkir);

        tvongkir.setText("(+ongkir Rp. "+getIntent().getStringExtra("ongkir")+",00 )");
        tvnamauser.setText("Halo "+userHelper_sqlite.getobject("nama"));
        tvtotal.setText("Total Dibayar : Rp. "+getIntent().getStringExtra("subtotal")+",00");
        tvtanggal.setText("Salam, Laundrypedia");

        RVdetail = findViewById(R.id.LVlist);

        RVdetail.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RVdetail.setLayoutManager(mLayoutManager);

        madapter = new adapter_detail_layanan(getApplicationContext(), Item_Laundry);
        RVdetail.setAdapter(madapter);

        btnkomplain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent kp = new Intent(getApplicationContext(), FormKomplainActivity.class);
                kp.putExtra("idlaundry",getIntent().getStringExtra("idlaundry"));
                kp.putExtra("nofaktur", getIntent().getStringExtra("nofaktur"));
                startActivity(kp);
            }
        });

        btnkonfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //muncul dialog
                final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DetailNotaActivity.this);
                alertDialogBuilder.setTitle("Hai");
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_konfirm,null);
                alertDialogBuilder.setView(dialogView);

                konfirm = (Button) dialogView.findViewById(R.id.button6);

                konfirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //kirim
                        Req_konfirm(getIntent().getStringExtra("nofaktur"));
                    }
                });



                alertDialogBuilder.setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // form_input();
                                //

                            }


                        });
                alertDialogBuilder.show();
                //Showing the alert dialog

            }
        });

        }


    private void Req_HomePage(final String nofaktur){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_get_detail, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("nofaktur", nofaktur);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_detail_layanan item = new item_detail_layanan();

                item.setNamaDl(dataArray.getString("nama_layanan"));
                item.setJmlDL(dataArray.getString("qty"));
                item.setHargaDL(dataArray.getString("item_price"));

                //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                Item_Laundry.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Req_konfirm(final String nofaktur){
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_Konfirm, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){
                        //save
                        DialogPesan("Notification", "Success Confirmed");


                    }
                    else {
                        DialogPesan("Login Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("nofaktur", nofaktur);



                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);
    }

    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        finish();
                    }
                })
                .show();
    }
}