package myapp.com.laundrypedia.activity;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.list_menu_help;

public class InformasiActivity extends AppCompatActivity {
    ListView lsmenu;
    list_menu_help adap;
    String item[] = {

            "Tentang Laundrypedia",
            "Tentang Komplain",
            "Pusat Bantuan",

    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("HELP");

        setContentView(R.layout.activity_help);
        lsmenu = findViewById(R.id.listmenu);
        adap = new list_menu_help(this, item);
        lsmenu.setAdapter(adap);

        lsmenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String pilihmenu = item[+i];
                if (pilihmenu.equals("Tentang Laundrypedia")) {


                } else if (pilihmenu.equals("Tentang Komplain")) {
                    Intent komp = new Intent(getApplicationContext(), PeraturanKomplainActivity.class);
                    startActivity(komp);

                } else if (pilihmenu.equals("Pusat Bantuan")) {

                }


            }
        });

    }

    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
}