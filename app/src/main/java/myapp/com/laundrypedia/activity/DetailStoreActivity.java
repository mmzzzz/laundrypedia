package myapp.com.laundrypedia.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.adapter_layanan;
import myapp.com.laundrypedia.adapter.adapter_paket_all;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;
import myapp.com.laundrypedia.model.item_laundry;
import myapp.com.laundrypedia.model.item_layanan;

public class DetailStoreActivity extends AppCompatActivity {
    RecyclerView RVLayanan;
    adapter_layanan madapter;
    private List<item_layanan> ItemLayanan = new ArrayList<item_layanan>();
    ProgressDialog pDialog;
    private static String TAG="DetailStoreActivity";
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected DetailStoreActivity.LayoutManagerType mCurrentLayoutManagerType;


    TextView tvNama, tvAlamat, tvJam;
    ImageView imglaundry;
    Button btntotal;
    UserHelper_sqlite userHelper_sqlite;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Store Detail");
        pDialog = new ProgressDialog(this);
        Req_HomePage();
        setContentView(R.layout.activity_detail_laundry);
        InitView();
        userHelper_sqlite = new UserHelper_sqlite(getApplication());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            //hapus
            askDelete();

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //jika diskip, maka
        //this.finish();
        askDelete();

    }

    private void InitView(){

        btntotal = findViewById(R.id.btntotal);
        btntotal.setText("NEXT");


        btntotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //next
                Intent back = new Intent(getApplicationContext(), InputNextOrderActivity.class);
                back.putExtra("idbinatu", getIntent().getStringExtra("idbinatu"));
                back.putExtra("gambar","gambar");
                back.putExtra("nama", getIntent().getStringExtra("nama"));
                back.putExtra("alamat", getIntent().getStringExtra("alamat"));
                back.putExtra("jam_open", getIntent().getStringExtra("jam_open"));
                back.putExtra("jam_close",getIntent().getStringExtra("jam_close"));
                back.putExtra("ongkir", getIntent().getStringExtra("ongkir"));

                startActivity(back);

            }
        });

        tvNama = findViewById(R.id.tvnama);
        tvAlamat = findViewById(R.id.tvalamat);
        tvJam = findViewById(R.id.tvjam);
        imglaundry = findViewById(R.id.DT_Img);

        tvNama.setText(getIntent().getStringExtra("nama"));
        tvAlamat.setText(getIntent().getStringExtra("alamat"));
        tvJam.setText(getIntent().getStringExtra("jam_open")+" - "+getIntent().getStringExtra("jam_close"));

        Glide
                .with(getApplicationContext())
                .load(getIntent().getStringExtra("gambar"))
                .asBitmap()
                .placeholder(R.drawable.ic_slide2)
                .into(imglaundry);
        RVLayanan = findViewById(R.id.RVlayanan);

        RVLayanan.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RVLayanan.setLayoutManager(mLayoutManager);

        madapter = new adapter_layanan(getApplicationContext(), ItemLayanan);
        RVLayanan.setAdapter(madapter);
    }

    private void Req_HomePage(){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_Layanan, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idbinatu", getIntent().getStringExtra("idbinatu"));

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_layanan item = new item_layanan();
                item.setIdbinatu(getIntent().getStringExtra("idbinatu"));
                item.setNamaLy(dataArray.getString("nama_layanan"));
                item.setGambarLy(dataArray.getString("picture"));
                item.setHargaLy(dataArray.getString("item_price"));
                item.setIdLayanan(dataArray.getString("id"));
                item.setSatuan(dataArray.getString("unit"));
                 //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                ItemLayanan.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void askDelete(){
        new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .content("Kembali Ke Menu Sebelumnya akan menghapus order ")
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        userHelper_sqlite.delete("tbl_transaksi_sementara");
                        Intent kehome = new Intent(getApplicationContext(), SemuaLaundryActivity.class);
                        startActivity(kehome);
                        finish();
                    }
                })
                .show();
    }
}