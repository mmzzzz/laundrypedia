package myapp.com.laundrypedia.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.list_menu_help;

public class PeraturanKomplainActivity extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Informasi Komplain");

        setContentView(R.layout.activity_peraturan_komplain);
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
}
