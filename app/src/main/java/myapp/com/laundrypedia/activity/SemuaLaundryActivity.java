package myapp.com.laundrypedia.activity;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.adapter.adapter_paket_all;
import myapp.com.laundrypedia.adapter.adapter_paket_laundry;
import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.model.item_laundry;

public class SemuaLaundryActivity extends AppCompatActivity {
    RecyclerView RVPaket;
    adapter_paket_all madapter;
    private List<item_laundry> Item_Laundry = new ArrayList<item_laundry>();
    ProgressDialog pDialog;
    private static String TAG="SemuaLaundryActivity";
    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    EditText edsearch;



    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER,
        STRAGGEREDGRID_LAYOUT_MANAGER
    }
    private static final int SPAN_COUNT = 2;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected SemuaLaundryActivity.LayoutManagerType mCurrentLayoutManagerType;
    SearchView search;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("All Laundry Stores");


        setContentView(R.layout.activity_show_all);
        pDialog = new ProgressDialog(this);
        Req_HomePage();
        RVPaket = findViewById(R.id.RVPaket);

        RVPaket.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RVPaket.setLayoutManager(mLayoutManager);

        madapter = new adapter_paket_all(getApplicationContext(), Item_Laundry);
        RVPaket.setAdapter(madapter);

        //InitView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            kehome();

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //jika diskip, maka
        //this.finish();
        kehome();

    }
    private void kehome(){
        Intent home = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(home);
        finish();
    }
    private void InitView(){

        /*search = findViewById(R.id.Home_ImgSearch);

        edsearch = findViewById(R.id.Home_edSearch);
        edsearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(edsearch.getText().toString().length() > 1){
                    final List<item_laundry> filteredModelList = filter(Item_Laundry, edsearch.getText().toString());
                    madapter.setFilter(filteredModelList);
                }
            }
        });*/



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        search.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        search.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                madapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                madapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

   /* @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        final List<item_laundry> filteredModelList = filter(Item_Laundry, newText);

        madapter.setFilter(filteredModelList);
        return true;
    }*/



    private void Req_HomePage(){
        showPDialog("Loading ..");
        // Creating volley request obj
        StringRequest Req_Absen = new StringRequest(Request.Method.POST,
                AppConfig.URL_SemuaLaundry, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {

                    JSONArray data = new JSONArray(response);
                    PasangData(data);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();
                //checking error koneksi
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("api_key", "1234");

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_Absen);
    }
    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
    private void PasangData(JSONArray data){
        try {
            for (int i=0; i<data.length();i++){
                JSONObject dataArray = data.getJSONObject(i);

                item_laundry item = new item_laundry();

                item.setNamaLaundry(dataArray.getString("store_name"));
                item.setAlamatLaundry(dataArray.getString("address"));
                item.setGambarLaundry(dataArray.getString("logo"));
                item.setLongLaundry(dataArray.getString("longitude"));
                item.setLatLaundry(dataArray.getString("latitude"));
                item.setJamClose(dataArray.getString("close_at"));
                item.setJamOpen(dataArray.getString("open_at"));
                item.setIdLaundry(dataArray.getString("id_binatu"));
                item.setBiayaOngkir(dataArray.getString("shipping_cost"));
                item.setJarakFreeOngkir(dataArray.getString("free_on_charge"));

                //item.setTarget(dataArray.getString("target_pengumuman"));
                //item.setWaktu(dataArray.getString("created_at"));
                //item.setImg_brand_url(Template.UrlPath.Biro+dataArray.getString("logo"));

                Item_Laundry.add(item);
            }
            madapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //filter
    private List<item_laundry> filter(List<item_laundry> models, String query) {
        query = query.toLowerCase();

        final List<item_laundry> filteredModelList = new ArrayList<>();
        for (item_laundry model : models) {
            final String alamat = model.getAlamatLaundry().toLowerCase();
            final String jamclose = model.getJamClose().toLowerCase();
            final String jamopen = model.getJamOpen().toLowerCase();
            final String nama = model.getNamaLaundry().toLowerCase();


            if(alamat.contains(query) || jamclose.contains(query) || jamopen.contains(query) || nama.contains(query)){
                filteredModelList.add(model);
            }

        }
        return filteredModelList;
    }
}