package myapp.com.laundrypedia.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import myapp.com.laundrypedia.bridge.AppConfig;
import myapp.com.laundrypedia.bridge.AppController;
import myapp.com.laundrypedia.helper.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;

public class InputNextOrderActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    EditText deliv_addr, deliv_date, deliv_time;
    EditText pick_addr, pick_date, pick_time;
    Button btnsend;

    private String strDatePickup = "";
    private String strDateDelivery = "";
    UserHelper_sqlite userHelper_sqlite;

    private static String TAG = "InputOrderActivity";
    TextView tvtotal, tvsub, tvongkir;

    Date c;
    String formattedDate;
    SimpleDateFormat df;

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_order);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        SetJudul("Input Detail Order");
        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        pDialog = new ProgressDialog(this);
        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c);

        deliv_addr = findViewById(R.id.editText);
        deliv_date = findViewById(R.id.editText2);
        deliv_time = findViewById(R.id.editText3);

        pick_date = findViewById(R.id.editText4);
        pick_time = findViewById(R.id.editText5);
        pick_addr = findViewById(R.id.edpickadd);
        btnsend = findViewById(R.id.button9);



        tvtotal = findViewById(R.id.tvtotal);
        tvsub = findViewById(R.id.tvsubtotal);
        tvongkir = findViewById(R.id.textView31);

        tvtotal.setText("Rp. "+userHelper_sqlite.gettotalharga());
        tvongkir.setText("Rp. "+String.format("%.1f",Double.parseDouble(getIntent().getStringExtra("ongkir"))));

        double sub = Double.parseDouble(userHelper_sqlite.gettotalharga()) + Double.parseDouble(getIntent().getStringExtra("ongkir"));
        tvsub.setText("Rp. "+String.format("%.0f",sub));


        //Toast.makeText(getApplicationContext(), ""+userHelper_sqlite.gettotalharga(), Toast.LENGTH_LONG).show();

        deliv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonthYearPicker("deliv");
            }
        });

        pick_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonthYearPicker("pickup");
            }
        });

        deliv_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        pick_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //Log.d("")

        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendOrder(deliv_addr.getText().toString(),deliv_date.getText().toString(), deliv_time.getText().toString(),pick_addr.getText().toString(), pick_date.getText().toString(),pick_time.getText().toString());
            }
        });

        final String json = userHelper_sqlite.composeJSONfromSQLite(); //mengambil list pesanan detail
        Log.d("JSON PESANAN : ", json);

    }

    private void MonthYearPicker(final String type) {
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                if (type.equals("deliv")) {
                    deliv_date.setText(dateFormatter.format(newDate.getTime()));

                } else {
                    pick_date.setText(dateFormatter.format(newDate.getTime()));
                }
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }
    private void  showPDialog(String pesan){
        if (!pDialog.isShowing())
            pDialog.setMessage(pesan);
        pDialog.show();
    }
    private void hidePDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void DialogPesan(String judul, String pesan){
        new MaterialDialog.Builder(this)
                .title(judul)
                .content(pesan)
                .positiveText("OK")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //event ketika ok di klik
                        Intent kehome = new Intent(getApplicationContext(), SemuaLaundryActivity.class);
                        startActivity(kehome);
                        finish();
                    }
                })
                .show();
    }


    private void SendOrder(final String strdelivaddres, final String strdelivdate, final String strdelivtime,final String strpckaddr, final String strpickdate, final String strpicktime){
        showPDialog("Loading ..");
        // Creating volley request obj
        Log.d("PICK DATE", strpickdate);
        StringRequest Req_register = new StringRequest(Request.Method.POST,
                AppConfig.URL_InputOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                hidePDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    String KodeRespon = jObj.getString("response");

                    if(KodeRespon.equals("00")){

                        //save
                       DialogPesan("Sukses", "Anda Berhasil Melakukan Order, Cek Status Pesanan di History");
                    }
                    else {
                        DialogPesan("Proses Gagal", "Silakan ulangi");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //hidePDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                final String json = userHelper_sqlite.composeJSONfromSQLite(); //mengambil list pesanan detail
                Map<String, String> params = new HashMap<String, String>();

                params.put("pesanan", json);
                params.put("deliv_addr", strdelivaddres);
                params.put("deliv_date", strdelivdate);
                params.put("deliv_time", strdelivtime);
               // params.put("pick_date", strpickdate);
                params.put("pick_date", "2018-09-09");
                params.put("pick_time", strpicktime);
                params.put("pick_addr", strpckaddr);
                params.put("idbinatu",getIntent().getStringExtra("idbinatu"));
                params.put("subtotal", userHelper_sqlite.gettotalharga());
                params.put("email", userHelper_sqlite.getobject("email"));
                params.put("ongkir", getIntent().getStringExtra("ongkir"));

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(Req_register);

    }


    private void SetJudul(String s){
        getSupportActionBar().setTitle(s);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home){
            //.finish();
            Intent back = new Intent(getApplicationContext(), DetailStoreActivity.class);
            back.putExtra("idbinatu", getIntent().getStringExtra("idbinatu"));
            back.putExtra("gambar","gambar");
            back.putExtra("nama", getIntent().getStringExtra("nama"));
            back.putExtra("alamat", getIntent().getStringExtra("alamat"));
            back.putExtra("jam_open", getIntent().getStringExtra("jam_open"));
            back.putExtra("jam_close",getIntent().getStringExtra("jam_close"));
            startActivity(back);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //jika diskip, maka
        //this.finish();
        Intent back = new Intent(getApplicationContext(), DetailStoreActivity.class);
        back.putExtra("idbinatu", getIntent().getStringExtra("idbinatu"));
        back.putExtra("gambar","gambar");
        back.putExtra("nama", getIntent().getStringExtra("nama"));
        back.putExtra("alamat", getIntent().getStringExtra("alamat"));
        back.putExtra("jam_open", getIntent().getStringExtra("jam_open"));
        back.putExtra("jam_close",getIntent().getStringExtra("jam_close"));
        back.putExtra("ongkir", getIntent().getStringExtra("ongkir"));
        startActivity(back);
        finish();

    }


}
