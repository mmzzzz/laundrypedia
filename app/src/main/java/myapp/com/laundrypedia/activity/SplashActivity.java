package myapp.com.laundrypedia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import myapp.com.laundrypedia.R;
import myapp.com.laundrypedia.helper.UserHelper_sqlite;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 3;
    UserHelper_sqlite userHelper_sqlite;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        userHelper_sqlite = new UserHelper_sqlite(getApplication());
        Delay();

    }

    private void Delay(){
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                //cek dulu
                if(userHelper_sqlite.getobject("email") == null){
                    //jika tdk login
                    Intent kelog = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(kelog);
                    finish();
                }
                else {
                    //ke dashboard
                    Intent kelog = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(kelog);
                    finish();


                }
            }
        }, (SPLASH_DISPLAY_LENGTH*1000));
    }

}

